# Alex's Extremely Insecure Buildroot Overlays

| :warning: WARNING                                                              |
|:-------------------------------------------------------------------------------|
| These overlays are fundamentally insecure and remove security from your images |

Buildroot Overlays are a mechanism for adding files to your final
buildroot image. Simply point BR2_ROOTFS_OVERLAY at one of the
sub-directories to tweak your final image.

They are intended to make it easier to build test cases and other such
things where security isn't really the major concern.

** Available overlays

| name                  | purpose                                            |
+-----------------------+----------------------------------------------------+
| noauth-serial-and-ssh | Auto login to shell, allow empty passwords for ssh |
